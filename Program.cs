﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORETransactionsClient
{
    class Program
    {
        static void Main(string[] args)
        {
            CoreRule.CORETransactionsClient client = new CoreRule.CORETransactionsClient();
            //dont forget to change url endpoint at app.config
            client.BatchResultsAckSubmitTransaction(new CoreRule.COREEnvelopeBatchResultsAckSubmission()
            {
                ReceiverID = "123",
            });
        }
    }
}
